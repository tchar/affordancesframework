package gr.ntua.softeng.affordances.framework.evaluator.compare;

import gr.ntua.softeng.affordances.framework.evaluator.models.LeafData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

/**
 * Default implementation of {@link CompareCalculator}
 *
 * @author Tilemachos Charalampous
 * @see CompareCalculator
 */
public class DefaultCompareCalculator implements CompareCalculator{

	private final static Logger LOGGER = LoggerFactory.getLogger(DefaultCompareCalculator.class);

	/**
	 * Returns the initial double value as specified in
	 * {@link CompareCalculator#calculate(int, LeafData)}.
	 *
	 * @param compared					a {@link int}
	 * @param ld						a {@link LeafData}
	 * @return							a {@link double}
	 * @throws NullPointerException		if ld is null
	 * @see CompareCalculator
	 * @see CompareCalculator#calculate(int, LeafData)
	 */
	public double calculate(int compared, @Nonnull LeafData ld) {

		if (ld.getOp().getAcceptableCompareValues().contains(compared)){
			LOGGER.info("Resulted " + ld.getWeight());
			return ld.getWeight();
		}
		else{
			LOGGER.info("Resulted 0");
			return 0;
		}
	}
}
