package gr.ntua.softeng.affordances.framework.mapper;

/**
 * Exception thrown from a {@link Mapper}
 *
 * @author Tilemachos Charalampous
 */
public class MapperException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 *
	 * @param msg				{@link String}
	 */
	public MapperException(String msg){
		super(msg);
	}

	/**
	 *
	 * @param e					{@link Exception}
	 */
	public MapperException(Exception e){
		super(e);
	}

}
