package gr.ntua.softeng.affordances.framework.annotator.policy;

import gr.ntua.softeng.affordances.framework.evaluator.Evaluation;

import javax.annotation.Nonnull;
import java.util.Collection;

/**
 * Sample {@link AnnotationPolicy} implementation that keeps all
 * {@link Evaluation} objects.
 *
 * @author Tilemachos Charalampous
 * @see Evaluation
 */
public class PolicyKeepAll implements AnnotationPolicy {

    /**
     * Returns all the {@link Evaluation} objects passed to it.
     *
     * @param evaluations			    a {@link Collection} of {@link Evaluation}
     * @param <R>					    the affordance type encapsulated by the
     *           					    {@link Evaluation}
     * @return                          a {@link Collection} of {@link Evaluation}
     * @throws NullPointerException     if evaluations is null
     * @see Evaluation
     */
    @Override
    @Nonnull
    public <R> Collection<Evaluation<R>> filter(@Nonnull Collection<Evaluation<R>> evaluations) {
        return evaluations;
    }
}
