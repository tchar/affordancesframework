package gr.ntua.softeng.affordances.framework.mapper;

/**
 * Interface that extends {@link Mapper} in case
 * an implementation needs to map a resource.
 *
 * @author Tilemachos Charalampous
 * @see MapperException
 */
public interface ResourceMapper extends Mapper {

	/**
	 * Method that returns the name of the resource's class.
	 *
	 * @return				a {@link String}
	 */
	String getMapResourceClassName();

}
