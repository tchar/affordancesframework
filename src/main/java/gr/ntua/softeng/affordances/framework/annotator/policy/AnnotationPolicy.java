package gr.ntua.softeng.affordances.framework.annotator.policy;

import gr.ntua.softeng.affordances.framework.evaluator.Evaluation;
import gr.ntua.softeng.affordances.framework.evaluator.Evaluator;

import java.util.Collection;

/**
 * Interface that provides methods for filter evaluations.
 *
 * @author Tilemachos Charalampous
 * @see Evaluation
 * @see Evaluator
 */
public interface AnnotationPolicy {

	/**
	 * This method filters the evaluations from the {@link Evaluator}
	 *
	 * @param evaluations			a {@link Collection} of {@link Evaluation}
	 *                              objects to filter
	 * @param <R>					the affordance type encapsulated by the
	 *           					{@link Evaluation}
	 * @return						a {@link Collection} of {@link Evaluation}
	 * 								objects as the filter result.
	 * @see Evaluation
	 * @see Evaluator
	 */
	<R> Collection<Evaluation<R>> filter(Collection<Evaluation<R>> evaluations);
}
