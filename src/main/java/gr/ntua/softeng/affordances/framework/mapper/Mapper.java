package gr.ntua.softeng.affordances.framework.mapper;

/**
 * Interface for mapping urls. If any exception handling
 * needed there is a {@link MapperException}.
 *
 * @author Tilemachos Charalampous
 * @see MapperException
 */
public interface Mapper {

	/**
	 * method that returns the map's original path.
	 *
	 * @return			a {@link String}
	 */
	String getOriginalPath();

	/**
	 * method that returns the translated path.
	 *
	 * @return			a {@link String}
	 */
	String getTranslatedPath();

	/**
	 * method that returns the translated path
	 * containing the translated address.
	 *
	 * @return			a {@link String}
	 */
	String getTranslatedFullPath();


	/**
	 * method that returns the translated base address
	 * without any path.
	 *
	 * @return			a {@link String}
	 */
	String getTranslatedBaseAddress();

	/**
	 * method that returns the original base address
	 * without any path
	 *
	 * @return			a {@link String}
	 */
	String getOriginalBaseAddress();
}
