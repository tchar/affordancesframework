package gr.ntua.softeng.affordances.framework.evaluator.compare;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Default implementation of {@link Operation} interface.
 * This class implements all 5 compare operations of:
 * EQ (equality), GT (greater than), GE (greater or equal than),
 * LT (less than) and LE (less or equal than).
 *
 * @author Tilemachos Charalampous
 * @see Operation
 */
public class DefaultOperation implements Operation {

    private final Type op;

    /**
     * Constructor that gets as parameter a
     * {@link Type}.
     *
     * @param op                        a {@link Type}
     * @throws NullPointerException     if op is null
     */
    public DefaultOperation(@Nonnull Type op){
        this.op = op;
    }

    /**
     * Method implementation of
     * {@link Operation#getAcceptableCompareValues()}.
     * Basically it just returns
     * {@link Type#getAcceptableCompareValues()}.
     *
     * @return                      a {@link Set} of {@link Integer}
     *                              as the result of
     *                              {@link Type#getAcceptableCompareValues()}
     * @see Operation#getAcceptableCompareValues()
     * @see Type#getAcceptableCompareValues()
     */
    @Override
    @Nonnull
    public Set<Integer> getAcceptableCompareValues() {
        return op.getAcceptableCompareValues();
    }

    /**
     * Default operation types enum.
     * Supported types are EQ, GT, GE, LE, LT
     */
    public enum Type {
        EQ(new HashSet<>(Collections.singletonList(0))),
        GT(new HashSet<>(Collections.singletonList(1))),
        GE(new HashSet<>(Arrays.asList(0, 1))),
        LE(new HashSet<>(Arrays.asList(-1, 0))),
        LT(new HashSet<>(Collections.singletonList(-1)));

        private final Set<Integer> acceptable;

        /**
         * This is an enum constructor that initiates the acceptable values.
         * The initiation is performed automatically.
         *
         * @param acceptable				a {@link Set} of {@link Integer}
         */
        Type(Set<Integer> acceptable){
            this.acceptable = acceptable;
        }

        /**
         * method that returns all the acceptable compare values.
         *
         * @return							a {@link Set} of {@link Integer}
         * @see DefaultOperation#getAcceptableCompareValues()
         */
        @Nonnull
        public Set<Integer> getAcceptableCompareValues(){
            return this.acceptable;
        }

    }
}
