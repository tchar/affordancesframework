package gr.ntua.softeng.affordances.framework.context.manager;

import gr.ntua.softeng.affordances.framework.context.ContextKey;
import gr.ntua.softeng.affordances.framework.context.ContextValue;
import gr.ntua.softeng.affordances.framework.context.AffordanceContext;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Map;

/**
 * Interface that provides the methods for managing
 * a resource. The basic methods are
 * {@link ContextManager#populate(AffordanceContext, Object)}
 * and {@link ContextManager#compare(ContextKey, ContextValue, String)}
 * for populating {@link ContextValue} and comparing them with other
 * values.
 *
 * @author Tilemachos Charalampous
 * @see AffordanceContext
 * @see ContextKey
 * @see ContextValue
 */
public interface ContextManager {

    /**
     * Method that returns the Class object of the
     * resource that this {@link ContextManager} can
     * manage
     *
     * @return                                      a {@link Class} object
     */
    Class<?> getResourceClass();

    /**
     * Method that returns the fields of the resource
     * that this manager can manage.
     *
     * @return                                      a {@link Collection} of {@link String}
     */
    Collection<String> getResourceFields();

    /**
     * Method that returns the operation type each field supports.
     *
     * @param field                                 a {@link String} as the field name
     * @return                                      a {@link Map} from {@link String} to
     *                                              {@link Collection} of {@link String}.
     */
    Collection<String> getResourceFieldOperations(String field);

    /**
     * Method that populates the {@link AffordanceContext} by assigning
     * values to the appropriate {@link ContextValue} objects.
     *
     * @param context                               the {@link AffordanceContext}
     * @param object                                the {@link Object} resource data.
     * @throws UnsupportedOperationException        exception thrown if this manager
     *                                              does not support this method
     * @throws ContextManagerException              exception thrown if there is any problem
     *                                              with managing the given object.
     * @see AffordanceContext
     * @see ContextValue
     */
    void populate(AffordanceContext context, @Nullable Object object)
            throws UnsupportedOperationException, ContextManagerException;

    /**
     * Method that compares the given {@link ContextValue} with a {@link String}.
     * Any {@link ContextManager} implementation must do any conversions from
     * {@link String} to any other object, without throwing any other Exception
     * except {@link ContextManagerException}.
     *
     * @param contextKey                                a {@link ContextKey}
     * @param val1                                      a {@link ContextValue}
     * @param val2                                      a {@link String}
     * @return                                          an {@link int} as described by
     *                                                  {@link Comparable#compareTo(Object)}
     * @throws UnsupportedOperationException            exception thrown if the {@link ContextManager}
     *                                                  cannot support this method.
     * @throws ContextManagerException                  exception thrown if the {@link ContextManager}
     *                                                  encounters problems during the compare.
     * @see ContextKey
     * @see ContextValue
     * @see Comparable
     */
    int compare(ContextKey contextKey, ContextValue val1, String val2)
            throws UnsupportedOperationException, ContextManagerException;
}
