package gr.ntua.softeng.affordances.framework.evaluator;

import gr.ntua.softeng.affordances.framework.evaluator.models.AffordanceModel;
import gr.ntua.softeng.affordances.framework.evaluator.models.Model;
import gr.ntua.softeng.gm.reasoning.wfr.WFRReasoningModel;
import gr.ntua.softeng.affordances.framework.context.AffordanceContext;

import java.util.Collection;
import java.util.Map;

/**
 * Interface for the evaluator that provides the
 * {@link Evaluator#evaluate(AffordanceContext, Collection)}
 * method.
 *
 * @author Tilemachos Charalampous
 * @see AffordanceContext
 * @see Model
 * @see AffordanceModel
 */
public interface Evaluator {

    /**
     * Method that evaluates the given {@link Model} with the specific
     * {@link AffordanceContext} using the {@link WFRReasoningModel#applyReasoning(Map)}
     *
     * @param context               an {@link AffordanceContext}
     * @param models                a {@link Collection} of {@link Model}
     * @param <R>	                the affordance type.
     *                              This parameter can be virtually anything.
     * @return                      a {@link Collection} of {@link Evaluation}
     * @see AffordanceContext
     * @see Model
     * @see AffordanceModel
     * @see Evaluation
     * @see WFRReasoningModel
     */
    <R> Collection<Evaluation<R>> evaluate(AffordanceContext context, Collection<Model<?, R>> models);

}
