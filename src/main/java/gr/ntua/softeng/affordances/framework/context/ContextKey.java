package gr.ntua.softeng.affordances.framework.context;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Objects;

/**
 * Wrapper class that contains a resource name and resource field.
 * the resource name is a {@link String} that represents the name
 * of the resource class as in {@link Class#getName()} and the field
 * is a {@link String} tha represent an object's field name.
 *
 * @author Tilemachos Charalampous
 */
public class ContextKey {

    private String resourceName;
    private String resourceField;

    /**
     * Constructor of {@link ContextKey} that takes two parameters.
     * The resourceName parameter is a {@link String} that can be
     * obtained from {@link Class#getName()}.
     *
     * @param resourceName              a {@link String} that represents
     *                                  the resource's name
     * @param resourceField             a {@link String} that represents
     *                                  the resource's field.
     * @throws NullPointerException     if resourceName or resourceField is null
     */
    public ContextKey(@Nonnull String resourceName, @Nonnull String resourceField){
        this.resourceName = resourceName;
        this.resourceField = resourceField;
    }

    /**
     * Getter for resource name
     *
     * @return                      a {@link String} that represents
     *                              the resource's name.
     */
    @Nonnull
    public String getResourceName() {
        return resourceName;
    }

    /**
     * Getter for resource field
     *
     * @return                      a {@link String} that represents
     *                              the resource's field.
     */
    @Nonnull
    public String getResourceField() {
        return resourceField;
    }

    /**
     * Override {@link Object#equals(Object)} for proper {@link java.util.Set}
     * operations
     *
     * @param obj                   a {@link Object} to be compared
     * @return                      true if the objects have the same
     *                              resource name and resource field,
     *                              false otherwise.
     *                              Uses {@link Object#equals(Object)}
     */
    @Override
    public boolean equals(Object obj){
        if (obj == null) return false;

        if (!(obj instanceof ContextKey)) return false;

        ContextKey key = (ContextKey) obj;

        return Objects.equals(resourceName, key.resourceName) &&
                Objects.equals(resourceField, key.resourceField);
    }

    /**
     * Override {@link Object#hashCode()}  so the
     * {@link ContextKey} can be used properly in a {@link Collection}.
     *
     * @return                      a {@link int} that represents the
     *                              hashcode of this {@link ContextKey}.
     *                              Uses {@link Objects#hash(Object...)}.
     */
    @Override
    public int hashCode(){
        return Objects.hash(resourceName, resourceField);
    }

    /**
     * Override {@link Object#toString()}   so the
     * {@link ContextKey} can be used properly in a {@link Collection}.
     *
     * @return                      the {@link String} representation of
     *                              this {@link ContextKey}.
     */
    @Override
    public String toString(){
        String simplifiedResourceName = resourceName;
        int index = simplifiedResourceName.lastIndexOf(".");
        if (index != -1)
            simplifiedResourceName = simplifiedResourceName.substring(index+1);

        return simplifiedResourceName + "." + resourceField;
    }
}
