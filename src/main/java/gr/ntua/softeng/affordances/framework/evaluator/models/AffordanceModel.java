package gr.ntua.softeng.affordances.framework.evaluator.models;

import gr.ntua.softeng.affordances.framework.context.ContextKey;
import gr.ntua.softeng.affordances.framework.context.ContextValue;
import gr.ntua.softeng.affordances.framework.context.manager.ContextManager;
import gr.ntua.softeng.affordances.framework.context.manager.ContextManagerException;
import gr.ntua.softeng.gm.DataGoalModelNode;
import gr.ntua.softeng.gm.reasoning.wfr.LowHighNodeValue;
import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.framework.evaluator.EvaluationException;
import gr.ntua.softeng.affordances.framework.evaluator.compare.CompareCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Class that extends the {@link Model}. The reason behind this structure
 * is that you can extend {@link Model}, to create your own custom models,
 * or you can use this class for any models with custom {@link gr.ntua.softeng.gm.GoalModelNode}.

 * This class holds a {@link CompareCalculator} instance
 * to calculate compares resulting from
 * {@link ContextManager#compare(ContextKey, ContextValue, String)}.
 *
 * @param <R>	the affordance type. This parameter can be virtually anything.
 *
 * @author Tilemachos Charalampous
 */
public class AffordanceModel<R> extends Model<DataGoalModelNode<LeafData>, R> {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(AffordanceModel.class);

	private final CompareCalculator calculator;

	/**
	 * Constructor with parameters a name and a calculator.
	 * A name is needed to distinguish this model from other models.
	 *
	 * @param name									a {@link String}
	 * @param calculator							a {@link CompareCalculator}
	 * @throws NullPointerException					if calculator is null
	 * @see CompareCalculator
	 */
	public AffordanceModel(String name, @Nonnull CompareCalculator calculator){
		super(name);
		this.calculator = calculator;
	}

	/**
	 * Method that overrides {@link Model#getInitialValues(AffordanceContext)}
	 * and returns the initial values needed by the
	 * {@link gr.ntua.softeng.gm.reasoning.wfr.WFRReasoningModel}.
	 *
	 * @param context								a {@link AffordanceContext}
	 * @return										a {@link HashMap} of {@link String}
	 * 												and {@link LowHighNodeValue}
	 * @throws NullPointerException					if context is null
	 * @throws EvaluationException					throws exception if encounters
	 * 												problem during assignment.
	 * @see gr.ntua.softeng.gm.reasoning.wfr.WFRReasoningModel
	 * @see LowHighNodeValue
	 * @see AffordanceContext
	 */
	@Override
	@Nonnull
	protected Map<String, LowHighNodeValue> getInitialValues(@Nonnull AffordanceContext context) throws EvaluationException {
		
		Map<String, LowHighNodeValue> initialValues = new HashMap<>();
		
		Set<ContextKey> contextKeys = context.getKeys();
		
		for (DataGoalModelNode<LeafData> leaf: this.getLeafNodes()){
			LeafData ld = leaf.getData();
			if (ld == null){
				LOGGER.error("Found leaf without leaf data");
				throw new EvaluationException("Leaf with name \"" + leaf.getNodeName() + "\" has no data");
			}
			ContextKey contextKey = new ContextKey(ld.getResourceType(), ld.getResourceField());
			if (!contextKeys.contains(contextKey)){
				throw new EvaluationException("Model \"" + this.name + "\" does not contain key \"" + contextKey + "\"");
			}

			int compared;
			try {
				compared = context.compare(contextKey, context.getValue(contextKey), ld.getValue());
			} catch (ContextManagerException e) {
				LOGGER.error(e.getMessage());
				throw new EvaluationException(e.getMessage());
			}

			LOGGER.info("Compare: " + context.getValue(contextKey) + " " + ld.getOp() + " " + ld.getValue());
			double val = calculator.calculate(compared, ld);
			initialValues.put(leaf.getNodeName(), valueHelper.getValue(val));
		}
		
		return initialValues;
	}
}
