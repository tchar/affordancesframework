package gr.ntua.softeng.affordances.framework.evaluator;

import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.framework.evaluator.models.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Basic implementation of {@link Evaluator} that evaluates all {@link Model}
 * passed to {@link AffordanceEvaluator#evaluate(AffordanceContext, Collection)}.
 *
 * @author Tilemachos Charalampous
 * @see Evaluator
 * @see Evaluation
 * @see Model
 * @see AffordanceContext
 */
public class AffordanceEvaluator implements Evaluator{
	
	private final static Logger LOGGER = LoggerFactory.getLogger(AffordanceEvaluator.class);

	/**
	 * This method evaluates all given {@link Model} objects and returns a {@link List}
	 * of the corresponding {@link Evaluation} objects.
	 *
	 * @param context               	an {@link AffordanceContext}
	 * @param models                	a {@link Collection} of {@link Model}
	 * @param <R>						the affordance type.
	 *           						This parameter can be virtually anything.
	 * @return							a {@link List} of {@link Evaluation}
	 * @throws NullPointerException		if models is null
	 * @see Evaluation
	 * @see AffordanceContext
	 * @see Model
	 */
	public <R> List<Evaluation<R>> evaluate(AffordanceContext context, @Nonnull Collection<Model<?, R>> models){

		List<Evaluation<R>> ret = new LinkedList<>();
		for (Model<?, R> model: models){
			try{
				double result = model.applyReasoning(context);
				LOGGER.info("Reasoning result:" + result);
				ret.add(new Evaluation<>(model.getAffordance(), result));
				
			}catch (Exception ex){
				LOGGER.error(ex.getMessage());
			}
		}
		return ret;
	}
}
