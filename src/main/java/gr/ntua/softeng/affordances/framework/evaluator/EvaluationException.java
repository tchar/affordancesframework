package gr.ntua.softeng.affordances.framework.evaluator;

import gr.ntua.softeng.affordances.framework.evaluator.models.Model;
import gr.ntua.softeng.affordances.framework.context.AffordanceContext;

/**
 * Exception thrown from
 * {@link Model#getInitialValues(AffordanceContext)}
 * upon error.
 *
 * @author Tilemahchos Charalampous
 */
public class EvaluationException extends Exception {

    /**
     *
     * @param msg               a {@link String} message.
     */
    public EvaluationException(String msg){
        super(msg);
    }

    /**
     *
     * @param e                 an {@link Exception}.
     */
    public EvaluationException(Exception e){
        super(e);
    }
}
