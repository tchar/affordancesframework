package gr.ntua.softeng.affordances.framework.annotator;

import gr.ntua.softeng.affordances.framework.annotator.policy.AnnotationPolicy;
import gr.ntua.softeng.affordances.framework.annotator.policy.PolicyKeepBest;
import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.framework.evaluator.Evaluation;
import gr.ntua.softeng.affordances.framework.evaluator.Evaluator;
import gr.ntua.softeng.affordances.framework.evaluator.models.Model;
import gr.ntua.softeng.affordances.framework.evaluator.models.ModelFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Default implementation of {@link Annotator}. The constructor takes
 * a {@link ModelFactory} and an {@link Evaluator}.
 * {@link AffordanceAnnotator#getAffordances(AffordanceContext, AnnotationPolicy)}
 * uses the {@link ModelFactory} to get all models that are relative to the
 * specified {@link AffordanceContext}, evaluates them using the
 * {@link Evaluator} provided and filters them with the
 * {@link AnnotationPolicy} provided. If the {@link AnnotationPolicy}
 * provided is null then the default {@link PolicyKeepBest} is used.
 *
 * @param <R>	the affordance type. This parameter can be virtually anything.
 *
 * @author Tilemachos Charalampous
 * @see ModelFactory
 * @see AffordanceContext
 * @see AnnotationPolicy
 * @see PolicyKeepBest
 */
public class AffordanceAnnotator<R> implements Annotator<R>{

    private ModelFactory<R> modelFactory;
    private Evaluator evaluator;

    /**
     * Constructor.
     *
     * @param modelFactory              a {@link ModelFactory}
     * @param evaluator                 an {@link Evaluator}
     * @throws NullPointerException     if either modelFactory
     *                                  or evaluator are null
     */
    public AffordanceAnnotator(@Nonnull ModelFactory<R> modelFactory, @Nonnull Evaluator evaluator){
        this.modelFactory = modelFactory;
        this.evaluator = evaluator;
    }

    /*
        Perform the calculation, with a default policy of PolicyKeepBest if policy is null.
     */
    private Collection<Evaluation<R>> calculate(AffordanceContext context, AnnotationPolicy policy){

        Collection<Model<?, R>> models = modelFactory.getModels(context);
        Collection<Evaluation<R>> evaluations = evaluator.evaluate(context, models);

        if (policy == null){
            policy = new PolicyKeepBest();
        }

        return policy.filter(evaluations);
    }

    /**
     * Method to get all the affordances of type {@link R}. This method uses the {@link Evaluator}
     * provided to evaluate all {@link Model} objects. Any {@link Model} object is obtained
     * from {@link ModelFactory} provided. After the evaluation the {@link AnnotationPolicy}
     * to filter the results.
     *
     * @param context				    an {@link AffordanceContext}
     * @param policy				    a {@link AnnotationPolicy}
     * @return                          a {@link List} of {@link R}
     * @throws NullPointerException     if context is null
     * @see Evaluator
     * @see ModelFactory
     * @see AnnotationPolicy
     */
    @Override
    @Nonnull
    public List<R> getAffordances(@Nonnull AffordanceContext context, @Nullable AnnotationPolicy policy) {
        List<R> affordances = new LinkedList<>();
        Collection<Evaluation<R>> evals = calculate(context, policy);
        for (Evaluation<R> eval: evals){
            R affordance = eval.getAffordance();
            if (affordance == null)
                continue;
            affordances.add(affordance);
        }
        return affordances;
    }
}
