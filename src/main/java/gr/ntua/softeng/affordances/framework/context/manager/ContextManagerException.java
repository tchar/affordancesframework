package gr.ntua.softeng.affordances.framework.context.manager;

/**
 * Exception thrown from {@link ContextManager}, when
 * they encounter a problem.
 *
 * @author Tilemachos Charalampous
 * @see ContextManager
 */
public class ContextManagerException extends Exception{

    /**
     *
     * @param msg       a {@link String} as exception message.
     */
    public ContextManagerException(String msg){
        super(msg);
    }

    /**
     *
     * @param e         an {@link Exception}
     */
    public ContextManagerException(Exception e){
        super(e);
    }
}
