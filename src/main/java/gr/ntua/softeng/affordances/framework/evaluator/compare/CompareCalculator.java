package gr.ntua.softeng.affordances.framework.evaluator.compare;

import gr.ntua.softeng.affordances.framework.context.ContextKey;
import gr.ntua.softeng.affordances.framework.context.ContextValue;
import gr.ntua.softeng.affordances.framework.context.manager.ContextManager;
import gr.ntua.softeng.affordances.framework.evaluator.models.LeafData;

/**
 *  Interface that provides the leaf's initial value for the reasoner.
 *  The value is used by {@link gr.ntua.softeng.gm.reasoning.wfr.LowHighValueHelper}.
 *  to generate {@link gr.ntua.softeng.gm.reasoning.wfr.LowHighNodeValue}
 *  for {@link gr.ntua.softeng.gm.reasoning.wfr.WFRReasoningModel}.
 *
 * @author Tilemachos Charalampous
 * @see gr.ntua.softeng.gm.reasoning.wfr.LowHighValueHelper
 * @see gr.ntua.softeng.gm.reasoning.wfr.LowHighNodeValue
 * @see gr.ntua.softeng.gm.reasoning.wfr.WFRReasoningModel
 */
public interface CompareCalculator {

    /**
     * Method that returns a {@link double}, usually from 0 to 100
     * that then can be passed to {@link gr.ntua.softeng.gm.reasoning.wfr.LowHighValueHelper}
     * to generate a {@link gr.ntua.softeng.gm.reasoning.wfr.LowHighNodeValue}
     * for the {@link gr.ntua.softeng.gm.reasoning.wfr.WFRReasoningModel}.
     * It takes as arguments a {@link int} that is the result of
     * {@link ContextManager#compare(ContextKey, ContextValue, String)}
     * and a {@link LeafData}.
     *
     * @param compared                  a {@link int}
     * @param ld                        a {@link LeafData}
     * @return                          a {@link double}
     * @see gr.ntua.softeng.gm.reasoning.wfr.LowHighValueHelper
     * @see gr.ntua.softeng.gm.reasoning.wfr.LowHighNodeValue
     * @see gr.ntua.softeng.gm.reasoning.wfr.WFRReasoningModel
     * @see ContextManager#compare(ContextKey, ContextValue, String)
     */
    double calculate(int compared, LeafData ld);
}
