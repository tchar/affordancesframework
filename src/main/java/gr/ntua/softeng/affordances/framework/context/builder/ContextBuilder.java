package gr.ntua.softeng.affordances.framework.context.builder;

import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.framework.context.ContextKey;
import gr.ntua.softeng.affordances.framework.context.manager.ContextManager;

import java.util.Collection;

/**
 * This is the base interface for building {@link AffordanceContext}
 * objects. Any {@link AffordanceContext} builder should implement this
 * interface.
 *
 * @author Tilemachos Charalampous
 * @see AffordanceContext
 */
public interface ContextBuilder {

    /**
     * Returns an AffordanceContext object with the specified ContextKey
     * conjunctions and ContextManagers.
     *
     * @return          the {@link AffordanceContext}
     * @see             AffordanceContext
     */
    AffordanceContext build();

    /**
     * Adds the given keys to the CNF list. The CNF list is an objects
     * that formulate a CNF. Each key in keys is in disjunction with each
     * other and keys are in conjunction with any other keys passed in
     * different calls of this method.
     *
     * @param keys          A {@link Collection} of {@link ContextKey} to
     *                      add to cnf
     * @return              this {@link ContextBuilder}
     * @see                 ContextKey
     */
    ContextBuilder addConjunction(Collection<ContextKey> keys);

    /**
     * Adds the given keys to the CNF list. The CNF list is an objects
     * that formulate a CNF. Each key in keys is in disjunction with each
     * other and keys are in conjunction with any other keys passed in
     * different calls of this method.
     *
     * @param keys          A {@link ContextKey[]} to add to cnf
     * @return              this {@link ContextBuilder}
     * @see                 ContextKey
     */
    ContextBuilder addConjunction(ContextKey... keys);

    /**
     * Sets the given {@link ContextManager}s as the default managers of the
     * {@link AffordanceContext}. Each {@link ContextManager} is responsible for
     * managing a resource.
     *
     * @param managers      a {@link Collection} of {@link ContextManager}
     * @return              this {@link ContextBuilder}
     * @see                 ContextManager
     */
    ContextBuilder setManagers(Collection<ContextManager> managers);

    /**
     * Sets the given {@link ContextManager}s as the default managers of the
     * {@link AffordanceContext}. Each {@link ContextManager} is responsible for
     * managing a resource.
     *
     * @param managers      a {@link ContextManager[]}
     * @return              this ContextBuilder
     * @see                 ContextManager
     */
    ContextBuilder setManagers(ContextManager... managers);
}
