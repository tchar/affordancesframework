package gr.ntua.softeng.affordances.framework.evaluator.models;

import gr.ntua.softeng.affordances.framework.context.AffordanceContext;

import java.util.Collection;

/**
 * Interface that provides a method for getting {@link Model} objects
 * from an {@link AffordanceContext}. The {@link Model} can be of any
 * subclass of {@link Model} with the same affordance type {@link R}
 *
 * @param <R>	the affordance type. This parameter can be virtually anything.
 *
 * @author Tilemachos Charalampous
 * @see Model
 * @see AffordanceModel
 * @see AffordanceContext
 */
public interface ModelFactory<R> {

    /**
     * Returns all {@link Model} objects associated with the
     * provided {@link AffordanceContext}.
     *
     * @param context               a {@link AffordanceContext}
     * @return                      a {@link Collection} of {@link Model}
     * @see AffordanceContext
     * @see Model
     * @see AffordanceModel
     */
    Collection<Model<?, R>> getModels(AffordanceContext context);

}
