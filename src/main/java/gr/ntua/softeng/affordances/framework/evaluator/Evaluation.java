package gr.ntua.softeng.affordances.framework.evaluator;

import gr.ntua.softeng.affordances.framework.evaluator.models.Model;

import java.util.Collection;
import java.util.Objects;
import javax.annotation.Nonnull;

/**
 * Wrapper class for an evaluation result of a
 * {@link Model}.
 * This class holds the affordance of type {@link R} and its evaluation result.
 * This class implements the {@link Comparable} interface, since
 * evaluations must be comparable.
 *
 * @param <R>	the affordance type. This parameter can be virtually anything.
 *
 * @author Tilemachos Charalampous
 */
public class Evaluation<R> implements Comparable<Evaluation<R>>{
	
	private R affordance;
	private Double result;

	/**
	 * Constructor with parameters the affordance
	 * of type {@link R} and its evaluation result.
	 *
	 * @param affordance		an {@link R}
	 * @param result			a {@link double}
	 */
	public Evaluation(R affordance, double result) {
		this.affordance = affordance;
		this.result = result;
	}

	/**
	 * getter for {@link R}.
	 *
	 * @return					a {@link R}.
	 */
	public R getAffordance() {
		return affordance;
	}

	/**
	 * setter for {@link R}
	 *
	 * @param affordance				a {@link R}.
	 */
	public void setAffordance(R affordance) {
		this.affordance = affordance;
	}

	/**
	 * getter for the result.
	 *
	 * @return					a {@link double}.
	 */
	public Double getResult() {
		return result;
	}

	/**
	 * setter for the result.
	 *
	 * @param result			a {@link double}.
	 */
	public void setResult(double result) {
		this.result = result;
	}


	/**
	 * Override the {@link Comparable#compareTo(Object)} method.
	 *
	 * @param e					a {@link Evaluation} to be compared.
	 * @return					an {@link int} as in
	 * 							{@link Comparable#compareTo(Object)}.
	 */
	@Override
	public int compareTo(@Nonnull Evaluation<R> e) {
		return this.result.compareTo(e.result);
	}

	/**
	 * Override the {@link Object#equals(Object)} so the
	 * {@link Evaluation} can be used properly in a {@link Collection}.
	 *
	 * @param o					a {@link Object} to be compared.
	 * @return					true if this {@link Evaluation}
	 * 							has the same {@link R}
	 * 							and the result is the same.
	 * 							Uses {@link Objects#equals(Object, Object)}.
	 */
	@Override
	public boolean equals(Object o){

		if (o == null) return false;
		if (!(o instanceof Evaluation)) return false;

		Evaluation e = (Evaluation) o;

		return Objects.equals(this.affordance, e.affordance) &&
				Objects.equals(this.result, e.result);
	}

	/**
	 * Override the {@link Object#hashCode()} so the
	 * {@link Evaluation} can be used properly in a {@link Collection}.
	 *
	 * @return					a {@link int} that represents the hashcode
	 * 							of this {@link Evaluation}.
	 * 							Uses {@link Objects#hash(Object...)} with
	 * 							parameters an {@link R}
	 * 							and a {@link double} as the result.
	 */
	@Override
	public int hashCode(){
		return Objects.hash(this.affordance, this.result);
	}

}
