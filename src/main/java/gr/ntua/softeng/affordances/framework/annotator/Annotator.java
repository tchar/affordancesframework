package gr.ntua.softeng.affordances.framework.annotator;

import gr.ntua.softeng.affordances.framework.annotator.policy.AnnotationPolicy;
import gr.ntua.softeng.affordances.framework.context.AffordanceContext;

import java.util.Collection;

/**
 * Interface for the annotator. This interface is designed to provide
 * methods for getting annotations of type {@link R}.
 *
 * @param <R>	the affordance type. This parameter can be virtually anything.
 *
 * @author Tilemachos Charalampous
 */
public interface Annotator<R> {

	/**
	 * This method returns a {@link Collection} of {@link R} objects based on a
	 * {@link AffordanceContext} and an {@link AnnotationPolicy}.
	 *
	 * @param context				an {@link AffordanceContext}
	 * @param policy				a {@link AnnotationPolicy}
	 * @return						a {@link Collection} of {@link R} objects.
	 * @see AffordanceContext
	 * @see AnnotationPolicy
	 */
	Collection<R> getAffordances(AffordanceContext context, AnnotationPolicy policy);
}
