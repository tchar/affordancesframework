package gr.ntua.softeng.affordances.framework.evaluator.models;

import gr.ntua.softeng.affordances.framework.evaluator.compare.Operation;

/**
 * Wrapper class tha holds leaf data.
 *
 * @author Tilemachos Charalampous
 */
public class LeafData {
	private String resourceType;
	private String resourceField;
	private Operation op;
	private String value;
	private Double weight;

	/**
	 * getter for resource type. Resource type is a {@link String}
	 * that can be obtained by calling a resource's {@link Class#getName()}
	 * method.
	 *
	 * @return						a {@link String}
	 * @see Class#getName()
	 */
	public String getResourceType() {
		return resourceType;
	}

	/**
	 * setter for resource type. Resource type is a {@link String}
	 * that can be obtained by calling a resource's {@link Class#getName()}
	 * method.
	 *
	 * @param resourceType			a {@link String}
	 */
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	/**
	 * getter for resource field name.
	 *
	 * @return						a {@link String}
	 */
	public String getResourceField() {
		return resourceField;
	}

	/**
	 * setter for resource field name.
	 *
	 * @param resourceField			a {@link String}
	 */
	public void setResourceField(String resourceField) {
		this.resourceField = resourceField;
	}

	/**
	 * getter for op. op is an {@link Operation}.
	 *
	 * @return						a {@link Operation}
	 */
	public Operation getOp() {
		return op;
	}

	/**
	 * setter for op. op is an {@link Operation}.
	 *
	 * @param op					a {@link Operation}
	 */
	public void setOp(Operation op) {
		this.op = op;
	}

	/**
	 * getter for value.
	 *
	 * @return						a {@link String}
	 */
	public String getValue() {
		return value;
	}

	/**
	 * setter for value.
	 *
	 * @param value					a {@link String}
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * getter for weight.
	 *
	 * @return						a {@link double}.
	 */
	public Double getWeight() {
		return weight;
	}

	/**
	 * setter for weight.
	 *
	 * @param weight				a {@link double}.
	 */
	public void setWeight(Double weight) {
		this.weight = weight;
	}
}