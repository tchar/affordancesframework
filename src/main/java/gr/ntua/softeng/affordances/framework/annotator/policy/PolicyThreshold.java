package gr.ntua.softeng.affordances.framework.annotator.policy;

import gr.ntua.softeng.affordances.framework.evaluator.Evaluation;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Sample {@link AnnotationPolicy} implementation that keeps the all
 * {@link Evaluation} objects with result over than a certain threshold.
 *
 * @author Tilemachos Charalampous
 * @see Evaluation#getResult()
 */
public class PolicyThreshold implements AnnotationPolicy{

    private double threshold;

    /**
     *
     * @param threshold                     a {@link Double}.
     * @throws IllegalArgumentException     if the threshold is not between 0 and 100.
     */
    public PolicyThreshold(@Nonnull Double threshold) throws IllegalArgumentException{
        if (threshold < 0 || threshold > 100){
            throw new IllegalArgumentException("threshold must be between 0 and 100");
        }
        this.threshold = threshold;
    }

    /**
     *
     * @param evaluations			a {@link Collection} of {@link Evaluation}
     *                              objects to filter
     * @param <R>                   the affordance type.
     * @return                      all {@link Evaluation} objects that have result
     *                              over the given threshold in descending order.
     */
    @Override
    public <R> Collection<Evaluation<R>> filter(Collection<Evaluation<R>> evaluations) {
        List<Evaluation<R>> evals = new ArrayList<>(evaluations);
        evals.sort(Comparator.reverseOrder());
        return evals.stream()
                .filter(e -> e.getResult() > this.threshold)
                .collect(Collectors.toList());
    }
}
