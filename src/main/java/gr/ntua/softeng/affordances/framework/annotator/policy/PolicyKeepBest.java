package gr.ntua.softeng.affordances.framework.annotator.policy;

import gr.ntua.softeng.affordances.framework.evaluator.Evaluation;

import javax.annotation.Nonnull;
import java.util.*;

/**
 * Sample {@link AnnotationPolicy} implementation that keeps the best
 * {@link Evaluation} based on {@link Evaluation#getResult()}.
 *
 * @author Tilemachos Charalampous
 * @see Evaluation
 */
public class PolicyKeepBest implements AnnotationPolicy {

    /**
     * Returns the best {@link Evaluation} based on highest
     * {@link Evaluation#compareTo(Evaluation)}.
     *
     * @param evaluations			    a {@link Collection} of {@link Evaluation}
     *                                  objects to filter
     * @param <R>					    the affordance type encapsulated by the
     *           					    {@link Evaluation}
     * @return                          a {@link Collection} of {@link Evaluation}
     *                                  of size 1 if evaluations has any element in it
     *                                  otherwise size 0
     * @throws NullPointerException     if evaluations is null
     * @see Evaluation
     */
    @Override
    @Nonnull
    public <R> Collection<Evaluation<R>> filter(@Nonnull Collection<Evaluation<R>> evaluations) {
        try{
            List<Evaluation<R>> ret = new LinkedList<>();
            ret.add(Collections.max(evaluations));
            return ret;
        } catch (NoSuchElementException e){
            return new LinkedList<>();
        }
    }
}
