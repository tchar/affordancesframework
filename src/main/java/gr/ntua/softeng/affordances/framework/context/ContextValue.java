package gr.ntua.softeng.affordances.framework.context;

import javax.annotation.Nonnull;

/**
 * Wrapper class for context value. The actual context value
 * is an {@link Object}.
 *
 * @author Tilemachos Charalampous
 */
public class ContextValue {

    private Object value;

    /**
     * Default constructor
     */
    public ContextValue(){
    }

    /**
     * Constructor that initializes the context value.
     *
     * @param value                     a {@link Object} that
     *                                  represents the context value.
     * @throws NullPointerException     if value is null
     */
    public ContextValue(@Nonnull Object value) {
        this.value = value;
    }

    /**
     * Getter for the value
     *
     * @return                          a {@link Object}
     */
    @Nonnull
    public Object getValue(){
        return this.value;
    }

    /**
     * Override {@link Object#toString()} for proper representation
     * of {@link ContextValue}.
     *
     * @return                          a {@link String} representation of the
     *                                  {@link ContextValue}
     */
    @Override
    public String toString(){
        return String.valueOf(value);
    }

}
