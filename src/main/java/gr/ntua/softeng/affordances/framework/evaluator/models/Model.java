package gr.ntua.softeng.affordances.framework.evaluator.models;

import gr.ntua.softeng.affordances.framework.evaluator.compare.CompareCalculator;
import gr.ntua.softeng.gm.DefaultGoalModelImpl;
import gr.ntua.softeng.gm.GenericGoalModel;
import gr.ntua.softeng.gm.GoalModelNode;
import gr.ntua.softeng.gm.reasoning.wfr.LowHighNodeValue;
import gr.ntua.softeng.gm.reasoning.wfr.LowHighValueHelper;
import gr.ntua.softeng.gm.reasoning.wfr.WFRReasoningModel;
import gr.ntua.softeng.gm.reasoning.wfr.WFRReasoningModelGenerator;
import gr.ntua.softeng.gm.reasoning.wfr.memfunc.HighMemFunc;
import gr.ntua.softeng.gm.reasoning.wfr.memfunc.LowMemFunc;
import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.framework.context.ContextKey;
import gr.ntua.softeng.affordances.framework.evaluator.EvaluationException;
import gr.ntua.softeng.affordances.framework.evaluator.Evaluator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * This class extends a parameterized {@link DefaultGoalModelImpl}
 *
 * This is the base class for the models. If any custom models are
 * required consider extending this class and implementing
 * {@link Model#getInitialValues(AffordanceContext)}.
 *
 * If any models with custom {@link GoalModelNode} are needed,
 * consider using the {@link AffordanceModel} implementation.
 *
 * The core method of this class is {@link Model#applyReasoning(AffordanceContext)}
 * which applies the reasoning using {@link WFRReasoningModel}.
 *
 * @param <T>	the parameter can be any class that extends {@link GoalModelNode}
 * @param <R>	the affordance type. This parameter can be virtually anything.
 *
 * @author Tilemachos Charalampous
 * @see GenericGoalModel
 * @see DefaultGoalModelImpl
 * @see AffordanceModel
 * @see AffordanceContext
 * @see WFRReasoningModel
 */
public abstract class Model<T extends GoalModelNode, R> extends DefaultGoalModelImpl<T> {
	
	@SuppressWarnings("unused")
	private final static Logger LOGGER = LoggerFactory.getLogger(Model.class);

	protected abstract Map<String, LowHighNodeValue> getInitialValues(AffordanceContext context) throws EvaluationException;
	
	protected String name;
	

	protected Set<ContextKey> contextKeys;
	
	protected static LowHighValueHelper valueHelper = new LowHighValueHelper(new LowMemFunc(10, 60), new HighMemFunc(90, 40));

	private R affordance;

	/**
	 * Constructor that takes as a parameter the model's name.
	 *
	 * @param name						a {@link String}
	 */
	public Model(String name){
		this.contextKeys = new HashSet<>();
		this.name = name;
	}

	/**
	 * Core method that is used by {@link Evaluator} implementations to apply reasoning
	 * to the model using {@link WFRReasoningModel}. The {@link AffordanceContext} is
	 * needed to provide the initial values.
	 *
	 * @param context					an {@link AffordanceContext}
	 * @return							a {@link double}
	 * @throws EvaluationException		exception thrown upon any problem.
	 * @see Evaluator
	 * @see WFRReasoningModel
	 * @see AffordanceContext
	 * @see Model#getInitialValues(AffordanceContext)
	 * @see CompareCalculator
	 */
	public double applyReasoning(AffordanceContext context) throws EvaluationException{
		try{
			Map<String, LowHighNodeValue> initialValues = getInitialValues(context);


//		 Print Initial Values
//		for (String nodeName : initialValues.keySet()) {
//			LOGGER.info("Initial value for node:\"" + nodeName + "\" is " + initialValues.get(nodeName));
//		}
//		 Apply reasoning

			/*
					WFRReasoningModel is Thread unsafe!
			 */
			WFRReasoningModelGenerator gen = new WFRReasoningModelGenerator();
			WFRReasoningModel reasoningModel = gen.generateReasoningModel(this);
			Map<String, LowHighNodeValue> results = reasoningModel.applyReasoning(initialValues);
//		 Print Results - defuzzification
//		for (String node : results.keySet()) {
//			LowHighNodeValue nodeValue = results.get(node);
//			LOGGER.info("Node: " + node + ", Value:" + nodeValue + ", DefuzValue:" + getValueHelper().getSatisfactionDegree(nodeValue));
//		}

			// In our case only one root is possible;
			T rootNode = super.getRootNodes().get(0);

			return getValueHelper().getSatisfactionDegree(results.get(rootNode.getNodeName()));

		} catch (Exception e){
			LOGGER.error(e.getMessage());
			throw new EvaluationException(e);
		}
	}

	/**
	 * getter for {@link R}.
	 *
	 * @return						an {@link R}
	 * @see R
	 */
	public R getAffordance(){
		return this.affordance;
	}

	/**
	 * setter for {@link R}.
	 *
	 * @param affordance 			an {@link R}
	 * @see R
	 */
	public void setAffordance(R affordance){
		this.affordance = affordance;
	}

	/**
	 * getter for {@link LowHighValueHelper}.
	 *
	 * @return						a {@link LowHighValueHelper}
	 * @see LowHighValueHelper
	 */
	@Nonnull
	public static LowHighValueHelper getValueHelper(){
		return valueHelper;
	}

	/**
	 * getter for model name.
	 *
	 * @return						a {@link String}
	 */
	public String getName() {
		return name;
	}

	/**
	 * setter for model name.
	 *
	 * @param name 					a {@link String}
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * getter for context keys.
	 *
	 * @return						a {@link Set} of {@link ContextKey}
	 * @see ContextKey
	 */
	@Nonnull
	public Set<ContextKey> getContextKeys() {
		return contextKeys;
	}

	/**
	 * setter for context keys
	 * @param contextKeys				a {@link Set} of {@link ContextKey}
	 * @throws NullPointerException		if contextKeys is null
	 * @see ContextKey
	 */
	public void setContextKeys(@Nonnull Set<ContextKey> contextKeys) {
		this.contextKeys = contextKeys;
	}

	/**
	 * method that adds a context key to the keys.
	 *
	 * @param contextKey				a {@link ContextKey}
	 * @throws NullPointerException		if contextKey is null
	 * @see ContextKey
	 */
	public void addContextKey(@Nonnull ContextKey contextKey){
		this.contextKeys.add(contextKey);
	}

	/**
	 * Override {@link Object#toString()}. This method returns a string
	 * representation of this model. The returned {@link String} is
	 * basically the model's name.
	 *
	 * @return					a {@link String}
	 */
	@Override
	public String toString(){
		return this.name;
	}
}
