package gr.ntua.softeng.affordances.framework.context;


import gr.ntua.softeng.affordances.framework.context.manager.ContextManager;
import gr.ntua.softeng.affordances.framework.context.manager.ContextManagerException;
import gr.ntua.softeng.affordances.framework.evaluator.models.AffordanceModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

/**
 * This class is responsible for holding all the context information about the affordance.
 * {@link AffordanceContext} maps a {@link ContextKey} object to a {@link ContextValue}.
 * Each {@link ContextKey} contains a resource name and resource field, whereas each
 * {@link ContextValue} contains the value of that specific {@link ContextKey}.
 * The values in {@link ContextValue}s are to be computed by the
 * {@link ContextManager}s passed in the {@link AffordanceContext#AffordanceContext(Collection)}
 *
 * This class' main purpose is to populate its {@link ContextValue} objects via
 * {@link AffordanceContext#populate(Object...)} and compare them with other values from the
 * Goal Model via {@link AffordanceContext#compare(ContextKey, ContextValue, String)}
 *
 * @author Tilemachos Charalampous
 * @see ContextKey
 * @see ContextValue
 * @see ContextManager
 * @see AffordanceModel
 */
public class AffordanceContext {

	private final static Logger LOGGER = LoggerFactory.getLogger(AffordanceContext.class);

	private Map<ContextKey, ContextValue> isDefinedBy;
	private Collection<Set<ContextKey>> cnfList;
	private Map<String, ContextManager> managers;

	/**
	 * Default constructor.
	 */
	public AffordanceContext(){
		this.isDefinedBy = new HashMap<>();
		this.cnfList = new LinkedList<>();
		this.managers = new HashMap<>();
	}

	/**
	 * Constructor that sets the given {@link Collection} of {@link ContextManager}
	 * as the default managers.
	 *
	 * @param managers					a {@link Collection} of
	 * 									{@link ContextManager} objects
	 * @throws NullPointerException		if managers is null
	 * @see ContextManager
	 */
	public AffordanceContext(@Nonnull Collection<ContextManager> managers){
		this();
		for (ContextManager manager: managers){
			String resourceName = manager.getResourceClass().getName();
			this.managers.put(resourceName, manager);
		}

	}

	/**
	 * Put a pair of {@link ContextKey}, {@link ContextValue} to the map
	 *
	 * @param key						A {@link ContextKey}
	 * @param value						A {@link ContextValue}
	 * @throws NullPointerException		if key is null
	 * @see ContextKey
	 * @see ContextValue
	 */
	public void put(@Nonnull ContextKey key, @Nullable ContextValue value){
		isDefinedBy.put(key, value);
	}

	/**
	 * Getter for the AffordanceContext's ContextKeys
	 *
	 * @return							The {@link Set} of current {@link ContextKey} objects in
	 * 									the AffordanceContext
	 */
	@Nonnull
	public Set<ContextKey> getKeys(){
		return isDefinedBy.keySet();
	}

	/**
	 * Getter for the ContextValue mapped to the specified ContextKey
	 *
	 * @param key 						A {@link ContextKey}
	 * @return							The {@link ContextValue} paired with the given key
	 * @throws NullPointerException		if key is null
	 * @see ContextKey
	 * @see ContextValue
	 */
	@Nullable
	public ContextValue getValue(@Nonnull ContextKey key){
		return isDefinedBy.get(key);
	}

	/**
	 * Checks if a ContextKey is contained in this AffordanceContext
	 *
	 * @param key						A {@link ContextKey} to be checked if it is
	 *                                  present in this {@link AffordanceContext}
	 * @return							true if this key is present in the
	 * 									{@link AffordanceContext} otherwise false.
	 * @see ContextKey
	 */
	public boolean containsKey(@Nullable ContextKey key){
		return isDefinedBy.containsKey(key);
	}

	/**
	 * Checks if the provided ContextKeys are present in the AffordanceContext
	 *
	 * @param keys						A {@link Set} of {@link ContextKey} objects
	 * @return							true if all keys are present in the
	 * 									{@link AffordanceContext} otherwise false.
	 * @throws NullPointerException		if keys is null
	 * @see ContextKey
	 */
	public boolean containsAllKeys(@Nonnull Set<ContextKey> keys){
		return this.isDefinedBy.keySet().containsAll(keys);
	}

	/**
	 * Adds the given keys to the CNF list. The CNF list is a {@link List} of
	 * {@link Set} of {@link ContextKey} that formulate a CNF. Each key in keys is in
	 * disjunction with each other and keys are in conjunction with
	 * any other keys passed in different calls of this method.
	 *
	 * @param keys          				A {@link Set} of {@link ContextKey} objects
	 *                                      to add to cnf
	 * @throws NullPointerException			if keys is null
	 * @see ContextKey
	 */
	public void addConjunction(@Nonnull Set<ContextKey> keys){
		this.cnfList.add(keys);
		for (ContextKey key: keys){
			this.isDefinedBy.putIfAbsent(key, new ContextValue());
		}
	}

	/**
	 * Check if the given {@link Set} of {@link ContextKey} satisfy the
	 * current CNF formula.
	 *
	 * @param contextKeys					A {@link Set} of {@link ContextKey} objects.
	 * @return								true if the CNF is satisfied, otherwise false.
	 * @throws NullPointerException			if contextKeys is null
	 * @see ContextKey
	 */
	public boolean satisfiesCnf(@Nonnull Set<ContextKey> contextKeys){
		for(Set<ContextKey> elemKeys: cnfList){
			boolean matched = false;
			for (ContextKey elemKey: elemKeys){
				if (contextKeys.contains(elemKey)){
					matched = true;
					break;
				}
			}
			if (!matched){
				return false;
			}
		}
		return true;
	}

	/**
	 * Getter for the string representation of the CNF formula.
	 *
	 * @return								A {@link String} representation
	 * 										of the CNF formula.
	 */
	@Nonnull
	public String cnfToString(){
		String ret = "";
		for (Set<ContextKey> keys: cnfList){
			if (!keys.isEmpty()) {
				ret += "( ";
			}
			for (ContextKey key: keys){
				ret += key + " v ";
			}
			if (!keys.isEmpty()){
				ret = ret.substring(0, ret.length() - 3);
				ret += " ) ^ ";
			}
		}
		if (!cnfList.isEmpty()){
			ret = ret.substring(0, ret.length() - 3);
		}
		return ret;
	}

	/**
	 * Method for populating the ContextValues with the given data.
	 *
	 * @param data							An Array of {@link Object}
	 * @return								this {@link AffordanceContext}
	 * @throws NullPointerException			if data is null
	 * @throws ContextManagerException      Exception thrown by {@link ContextManager}
	 * @see ContextKey
	 * @see ContextValue
	 * @see ContextManager
	 * @see ContextManagerException
	 */
	@Nonnull
	public AffordanceContext populate(@Nonnull Object... data) throws ContextManagerException {

		Map<String, Object> dataMap = new HashMap<>();
		for (Object d: data){
			dataMap.put(d.getClass().getName(), d);
		}

		for (ContextKey key: this.getKeys()){
			Object d = dataMap.get(key.getResourceName());
			ContextManager manager = managers.get(key.getResourceName());
			if (manager == null){
				continue;
			}
			try {
				manager.populate(this, d);
			}catch (ContextManagerException e){
				LOGGER.error(e.getMessage());
				throw new ContextManagerException(e);
			} catch (Exception e){
				LOGGER.error(e.getMessage());
			}
		}
		return this;
	}

	/**
	 * Method that compares a ContextValue with a String. the actual compare is
	 * handled by a ContextManager
	 *
	 * @param key								A {@link ContextKey}
	 * @param value1							The {@link ContextValue} related with the key
	 * @param value2							A {@link String} value to be compared with the
	 * 											{@link ContextValue}
	 * @return									an {@link int} as specified by the
	 * 											{@link Comparable#compareTo(Object)}
	 * @throws NullPointerException				if key is null
	 * @throws ContextManagerException			Exception thrown if no manager exists that can
	 * 											handle the compare, or if the {@link ContextManager}
	 * 											throws an Exception.
	 * @see ContextKey
	 * @see ContextValue
	 * @see ContextManager
	 * @see ContextManagerException
	 */
	public int compare(@Nonnull ContextKey key, @Nullable ContextValue value1, @Nullable String value2) throws ContextManagerException{
		ContextManager manager = managers.get(key.getResourceName());
		if (manager == null){
			throw new ContextManagerException("Did not find any ContextManager for \"" + key.getResourceName() + "\".");
		}
		return manager.compare(key, value1, value2);
	}
}
