package gr.ntua.softeng.affordances.framework.context.builder;

import gr.ntua.softeng.affordances.framework.context.AffordanceContext;
import gr.ntua.softeng.affordances.framework.context.ContextKey;
import gr.ntua.softeng.affordances.framework.context.manager.ContextManager;

import javax.annotation.Nonnull;
import java.util.*;

/**
 * This is the default implementation of {@link ContextBuilder} interface
 * Any {@link ContextBuilder} implementation may extend this class instead
 * of implementing it directly.
 *
 * {@link DefaultContextBuilder} can be used to build an {@link AffordanceContext}
 * object. In any case any customization for the building process is
 * needed, consider extending this ContextBuilder or implement
 * {@link ContextBuilder} directly.
 *
 * @author Tilemachos Charalampous
 * @see AffordanceContext
 */
public class DefaultContextBuilder implements ContextBuilder {

    private List<Set<ContextKey>> contextKeys;
    private List<ContextManager> managers;

    /**
     * Create a {@link DefaultContextBuilder} object.
     */
    public DefaultContextBuilder(){
        this.contextKeys = new LinkedList<>();
        this.managers = new LinkedList<>();
    }

    /**
     * Returns an {@link AffordanceContext} object with the specified
     * {@link ContextKey} conjunctions and {@link ContextManager}s.
     *
     * @return          the AffordanceContext
     * @see ContextKey
     * @see AffordanceContext
     */
    @Override
    @Nonnull
    public AffordanceContext build() {
        AffordanceContext context = new AffordanceContext(managers);
        for (Set<ContextKey> keys: contextKeys){
            context.addConjunction(keys);
        }
        return context;
    }

    /**
     * Adds the given keys to the CNF list. The CNF list is a {@link List} of
     * {@link Set} of {@link ContextKey} objects that formulate a CNF.
     * Each key in keys is in disjunction with each other and keys are
     * in conjunction with any other keys passed in different calls of this method.
     *
     * @param keys                          A {@link Collection} of {@link ContextKey} to add to cnf
     * @return                              this {@link ContextBuilder}
     * @throws NullPointerException			if keys is null
     * @see ContextKey
     */
    @Override
    @Nonnull
    public ContextBuilder addConjunction(@Nonnull Collection<ContextKey> keys) {
        this.contextKeys.add(new HashSet<>(keys));
        return this;
    }

    /**
     * Adds the given keys to the CNF list. The CNF list is a List of
     * ContextKey Sets that formulate a CNF. Each key in keys is in
     * disjunction with each other and keys are in conjunction with
     * any other keys passed in different calls of this method.
     *
     * @param keys                          A {@link ContextKey[]} to add to cnf
     * @return                              this {@link ContextBuilder}
     * @throws NullPointerException			if keys is null
     * @see ContextKey
     */
    @Override
    @Nonnull
    public ContextBuilder addConjunction(@Nonnull ContextKey... keys) {
        return this.addConjunction(Arrays.asList(keys));
    }

    /**
     * Sets the given {@link ContextManager}s as the default managers of the
     * {@link AffordanceContext}. Each {@link ContextManager} is responsible for
     * managing a resource.
     *
     * @param managers                      a {@link Collection} of {@link ContextManager}
     * @return                              this {@link ContextBuilder}
     * @throws NullPointerException	        if managers is null
     * @see ContextManager
     */
    @Override
    @Nonnull
    public ContextBuilder setManagers(@Nonnull Collection<ContextManager> managers){
        this.managers.addAll(managers);
        return this;
    }

    /**
     * Sets the given {@link ContextManager}s as the default managers of the
     * {@link AffordanceContext}. Each {@link ContextManager} is responsible for
     * managing a resource.
     *
     * @param managers                      a {@link ContextManager[]}
     * @return                              this ContextBuilder
     * @throws NullPointerException			if managers is null
     * @see ContextManager
     */
    @Override
    @Nonnull
    public ContextBuilder setManagers(@Nonnull ContextManager... managers){
        return this.setManagers(Arrays.asList(managers));
    }
}
