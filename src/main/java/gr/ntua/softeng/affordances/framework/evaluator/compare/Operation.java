package gr.ntua.softeng.affordances.framework.evaluator.compare;

import gr.ntua.softeng.affordances.framework.context.ContextKey;
import gr.ntua.softeng.affordances.framework.context.ContextValue;
import gr.ntua.softeng.affordances.framework.context.manager.ContextManager;

import java.util.Collection;

/**
 * Interface that provides acceptable compare values
 * for the {@link CompareCalculator}. This is not
 * required by any {@link CompareCalculator} implementation,
 * but is offered as a helper interface, to help with the
 * abstraction and implementation of the {@link CompareCalculator}.
 *
 * @author Tilemachos Charalampous
 * @see CompareCalculator
 * @see DefaultCompareCalculator
 */
public interface Operation {

    /**
     * Method that returns the acceptable {@link Integer}
     * values from a compare call as in {@link Comparable#compareTo(Object)}
     * and {@link ContextManager#compare(ContextKey, ContextValue, String)}.
     *
     * @return                  a {@link Collection} of {@link Integer}.
     * @see Comparable#compareTo(Object)
     * @see ContextManager#compare(ContextKey, ContextValue, String)
     * @see CompareCalculator
     * @see DefaultCompareCalculator
     */
    Collection<Integer> getAcceptableCompareValues();

}
