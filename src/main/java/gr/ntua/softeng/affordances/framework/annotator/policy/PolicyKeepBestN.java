package gr.ntua.softeng.affordances.framework.annotator.policy;

import gr.ntua.softeng.affordances.framework.evaluator.Evaluation;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 * Sample {@link AnnotationPolicy} implementation that keeps the best
 * N {@link Evaluation} objects based on {@link Evaluation#getResult()}.
 *
 * @author Tilemachos Charalampous
 */
public class PolicyKeepBestN implements AnnotationPolicy{

    private Integer best;

    /**
     * Constructor that takes as a parameter a non null
     * Integer with value greater than 0
     *
     * @param best                          an {@link Integer}
     * @throws NullPointerException         exception thrown when
     *                                      input is null
     * @throws IllegalArgumentException     exception thrown when
     *                                      input is less or equal than 0
     */
    public PolicyKeepBestN(@Nonnull Integer best) {
        if (best <= 0){
            throw new IllegalArgumentException("Best must be greater than 0");
        }
        this.best = best;
    }

    /**
     * Returns the best N {@link Evaluation} objects based on highest
     * {@link Evaluation#compareTo(Evaluation)}.
     *
     * @param evaluations			    a {@link Collection} of {@link Evaluation}
     *                                  objects to filter
     * @param <R>					    the affordance type encapsulated by the
     *                 					{@link Evaluation}
     * @return                          a {@link List} of {@link Evaluation} in
     *                                  descending order.
     * @throws NullPointerException     if evaluations is null
     * @see Evaluation
     */
    @Override
    @Nonnull
    public <R> List<Evaluation<R>> filter(@Nonnull Collection<Evaluation<R>> evaluations) {
        List<Evaluation<R>> evals = new ArrayList<>(evaluations);
        evals.sort(Comparator.reverseOrder());
        return evals.subList(0, Math.min(best, evals.size()));
    }
}
